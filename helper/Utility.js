/*
* Utility included some helper static function 
*/


function Utility(){
	
}


/**
* An enum object as argument and string as return value
* The enum need to have count defined
**/
Utility.enumToKeystonOptionStr = function(enumObj) {

	var enumsArray = enumObj.enums;

	var returnOptionString = "";
	for(var i=0;i<enumsArray.length;i++){
		returnOptionString += enumsArray[i].toString();

		if(i < enumsArray.length - 1){
			returnOptionString += ",";
		}
	}

	return returnOptionString;
};

/**
* Check if an string is within the key of enum
**/
Utility.isStringWithinEnum = function(stringToCheck, enumObj){
	console.log("stringToCheck: " + stringToCheck);
	var enumsArray = enumObj.enums;

	for(var i=0;i<enumsArray.length;i++){
		if(stringToCheck == enumsArray[i].toString()){
			return true;
		}
	}
	return false;
}

module.exports = Utility;