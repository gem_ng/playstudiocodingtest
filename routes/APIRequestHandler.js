var Enum = require('enum');


function APIRequestHandler (){
	if ( arguments.callee._singletonInstance )
		return arguments.callee._singletonInstance;
	arguments.callee._singletonInstance = this;

}


//Define the content type of the data from Req/ to Res
var ResContentTypeString = ["application/json;charset=utf-8"];
APIRequestHandler.prototype.ResContentType = new Enum([
	"JSON"
	]);


/***
*	resDataStr should be in JSON format 
*
***/
APIRequestHandler.prototype.sendRes = function(res, resDataStr,type){
	var encryptedDataAry = resDataStr;

	res.writeHead( 200, this.ResContentType[type.value]);

	var resStr = JSON.stringify(resDataStr);
	res.end(resStr);
}



module.exports = APIRequestHandler;