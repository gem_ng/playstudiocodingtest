var keystone = require('keystone');
var middleware = require('./middleware');

var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	api:  importRoutes('./api'),
	tests: importRoutes('./tests'),
};

// Setup Route Bindings
exports = module.exports = function(app) {
	
	// Views
	app.get('/', routes.views.index);
	app.post('/updatePlayerActionStats',routes.api.updatePlayerActionStats);
	app.post('/getAchievementRecord',routes.api.getAchievementRecord);
	app.post('/getAchievementItems',routes.api.getAchievementItems);
	
	app.get('/apitest',routes.tests.index);
};
