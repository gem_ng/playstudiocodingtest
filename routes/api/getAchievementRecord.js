var keystone = require('keystone');
var PlayerAchievementRecord = require(global.__base + '/models/PlayerAchievementRecord');
var APIRequestHandler = require(global.__base + '/routes/APIRequestHandler');

/* 
/ return array of achievementRecord  of a player in json
/ Req body should be playerId in json format.
/ {playerId:1}
/
/ returned Json Format
/ result  - 1/0  (success/failed)
/ data - achievementRecord objects in json.
/ failMsg - only show when there is a fail
*/
exports = module.exports = function(req, res) {
	var body = req.body;	
	
	//validate all the field before processs
	var playerId = body.playerId;
	var apiRequestHandler = new APIRequestHandler();
	if(playerId == undefined){
		apiRequestHandler.sendRes(
			res,
			{result:0,failMsg:"Missing Parameter"},
			apiRequestHandler.ResContentType.JSON.key
			);
	}
	else{
		var playerAchievementRecord = new PlayerAchievementRecord();
		playerAchievementRecord.getAchievementRecordForPlayer(
			playerId
			,function(err,achievementRecords){
				apiRequestHandler.sendRes(
					res,
					{result:1,data:JSON.stringify(achievementRecords)},
					apiRequestHandler.ResContentType.JSON.key
					);					
			});

	}
	
};
