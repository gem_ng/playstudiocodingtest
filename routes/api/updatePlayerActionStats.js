var keystone = require('keystone');
var async = require('async');
var PlayerActionStats = require(global.__base + '/models/PlayerActionStats');
var AchievementItem = require(global.__base + '/models/AchievementItem');
var PlayerAchievementRecord = require(global.__base + '/models/PlayerAchievementRecord');
var APIRequestHandler = require(global.__base + '/routes/APIRequestHandler');
var Utility = require(global.__base + '/helper/Utility');
var PlayerAction = require(global.__base + '/enum/PlayerAction');
/* 
/ Req body should be playerid, achievementId, countToAdd in json format.
/  {playerId:1,action:"login",countToAdd:2}
/
/ returned Json Format
/ result  - 1/0  (success/failed)
/ data - reward object in json.
/ failMsg - only show when there is a fail
*/
exports = module.exports = function(req, res) {
	var body = req.body;	
	//var bodyObj = JSON.parse(body);

	//validate all the field before processs
	var playerId = body.playerId;
	var action = body.action;
	var countToAdd = body.countToAdd;
	var apiRequestHandler = new APIRequestHandler();
	if(playerId == undefined || action == undefined || countToAdd == undefined){
		apiRequestHandler.sendRes(
			res,
			{result:0,failMsg:"Missing Parameter"},
			apiRequestHandler.ResContentType.JSON.key
			);
		return;
	}

	if(!Utility.isStringWithinEnum(action,PlayerAction)){
		apiRequestHandler.sendRes(
			res,
			{result:0,failMsg:"Action not exist"},
			apiRequestHandler.ResContentType.JSON.key
			);
		return;
	}

	
	//Get the player Action Stats , Achievement Item and player Achievement record 
	//related to playerId and action in parallel
	async.parallel([
		function(callback){
			var playerActionStats = new PlayerActionStats();
			playerActionStats.updateActionStatsForPlayer(playerId,action,countToAdd,callback);
			playerActionStats = null;
		}
		,function(callback){
			var achievementItem = new AchievementItem();
			achievementItem.getAllAchievementItemWithAction(action,callback);
			achievementItem = null;
		}
		,function(callback){
			var playerAchievementRecord = new PlayerAchievementRecord();
			playerAchievementRecord.getAchievementRecordForPlayerWithAction(playerId,action,callback);
			playerAchievementRecord = null;
		}
		],
		function(err,results){
			var playerActionStats = results[0];
			var achievementItems = results[1];
			var playerAchievementRecords = results[2];
			var gainedRewards = [];

			for(var i=0;i<achievementItems.length;i++){
				var tempAchievementItem = achievementItems[i];
				if(playerActionStats.count >= tempAchievementItem.countNeed){

					var isAchievementAlreadyReached = false;

					//check if the achievement already recorded previously
					for(var j=0;j<playerAchievementRecords.length;j++){
						var tempAchievementRecord = playerAchievementRecords[j];

						if(tempAchievementRecord.achievement.toString() == tempAchievementItem._id.toString()){
							isAchievementAlreadyReached = true;
							break;
						}
					}
					
					if(!isAchievementAlreadyReached){
						var playerAchievementRecord = new PlayerAchievementRecord();
						playerAchievementRecord.addAchievementRecordForPlayer(
							playerId,
							tempAchievementItem._id,
							tempAchievementItem.reward);
						gainedRewards.push(tempAchievementItem.reward);
					}
				}
			}

			apiRequestHandler.sendRes(
				res,
				{result:1,data:JSON.stringify(gainedRewards)},
				apiRequestHandler.ResContentType.JSON.key
				);	
			// console.log("err: " + JSON.stringify(err,"","\t"));
			// console.log("results: " + JSON.stringify(results,"","\t"));
			// console.log("gainedRewards: " + JSON.stringify(gainedRewards,"","\t"));
		});

};
