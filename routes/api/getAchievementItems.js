var keystone = require('keystone');
var PlayerAchievementItem = require(global.__base + '/models/AchievementItem');
var APIRequestHandler = require(global.__base + '/routes/APIRequestHandler');

/* 
/ return array of PlayerAchievementItems in json
/
/ returned Json Format
/ result  - 1/0  (success/failed)
/ data - reward object in json.
/ failMsg - only show when there is a fail
*/
exports = module.exports = function(req, res) {
	var body = req.body;	
	
	var apiRequestHandler = new APIRequestHandler();
	var playerAchievementItem = new PlayerAchievementItem();
	playerAchievementItem.getAllAchievementItem(
		function(err,allAchievementItems){
			
			apiRequestHandler.sendRes(
				res,
				{result:1,data:JSON.stringify(allAchievementItems)},
				apiRequestHandler.ResContentType.JSON.key
				);					
		});
};
