var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.section = 'apitests';


	var apiSampleValueArray = [];

	//updatePlayerActionStats
	{
		var tempAPI = {
			name:'Update PlayerActionStats (/updatePlayerActionStats)',
			action:'/updatePlayerActionStats',
			values: [
			{label:'Player ID(playerId): ',name:'playerId',value:'1'},
			{label:'Action(action): ',name:'action',value:'BlackJack'},
			{label:'Count To Add(countToAdd): ',name:'countToAdd',value:2}
			]
		};		
		apiSampleValueArray.push(tempAPI);
	}

	//getAchievementRecord
	{
		var tempAPI = {
			name:'Get AchievementRecord (/getAchievementRecord)',
			action:'/getAchievementRecord',
			values: [
			{label:'Player ID(playerId): ',name:'playerId',value:'1'}
			]
		};		
		apiSampleValueArray.push(tempAPI);
	}


	//getAchievementItems
	{
		var tempAPI = {
			name:'Get AchievementItems (/getAchievementItems)',
			action:'/getAchievementItems',
			values: []
		};		
		apiSampleValueArray.push(tempAPI);
	}
	// Render the view
	view.render('tests/index',{sampleValue:apiSampleValueArray});
	
};
