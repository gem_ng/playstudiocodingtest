var Enum = require("Enum");
var PlayerAction = new Enum({
	"BlackJack":0,
	"BetTimes":1,
	"Win":2,
	"Lose":3,
	"TotalLoginDay":4,
	"SpinAWheel":5
});

module.exports = PlayerAction;