exports.create = {
	Reward: [
	{ 
		"count" : 100, 
		"name" : "Chips100", 
		"rewardType" : "Chip"
	},
	{ 
		"count" : 200, 
		"name" : "Chips200", 
		"rewardType" : "Chip"
	},
	{ 
		"count" : 500, 
		"name" : "Chips500", 
		"rewardType" : "Chip"
	},
	{ 
		"count" : 1000, 
		"name" : "LoyalityPoint1000", 
		"rewardType" : "LoyalityPoint"
	},
	{ 
		"count" : 10000, 
		"name" : "LoyalityPoint10000", 
		"rewardType" : "LoyalityPoint"
	},
	{ 
		"count" : 50000, 
		"name" : "LoyalityPoint50000", 
		"rewardType" : "LoyalityPoint"
	}
	]
};