var keystone = require('keystone'),
	async = require('async'),
	AchievementItem = keystone.list('AchievementItem'),
	Reward = require(global.__base + "/models/Reward");


var AchievementItems = [
	{ 
		"achievementType" : "Permanent", 
		"countNeed" : 20, 
		"description" : "Black Jack for 20 times.",
		"name" : "Black Jack",
		"achievementAction" : "BlackJack",
		"reward": "Chips100"    //Need to map to object id of reward
	},
	{ 
		"achievementType" : "Permanent", 
		"countNeed" : 200, 
		"description" : "Black Jack for 50 Times", 
		"name" : "Black Jack Amateur", 
		"achievementAction" : "BlackJack",
		"reward": "Chips200"
	},
	{ 
		"achievementType" : "Permanent", 
		"countNeed" : 1000, 
		"description" : "Black Jack for 1000 times", 
		"name" : "Black Jack Pro", 
		"achievementAction" : "BlackJack",
		"reward": "Chips500"
	},
	{ 
		"achievementType" : "Permanent", 
		"countNeed" : 10, 
		"description" : "Spin a Wheel 10 times", 
		"name" : "Spinner", 
		"achievementAction" : "SpinAWheel",
		"reward": "LoyalityPoint1000"
	},
	{ 
		"achievementType" : "Daily", 
		"countNeed" : 3, 
		"description" : "Win 3 times a day", 
		"name" : "Gambler", 
		"achievementAction" : "Win",
		"reward": "LoyalityPoint1000"
	}
	];



function createAchievementItem(achievementIem, done) {
	
	
	async.waterfall([
		function(callback){
			var reward = new Reward();
			reward.getRewardWithName(achievementIem.reward,callback);
		},
		function(data,callback){
			achievementIem.reward = data._id;
			var newAchievementItem = new AchievementItem.model(achievementIem);
			newAchievementItem.save(callback);
		}
		],
		function(err){
			if (err) {
				console.error("Error adding achievementIem " + achievementIem.name + " to the database:");
				console.error(err);
			} else {
				console.log("Added achievementIem " + achievementIem.name + " to the database.");
			}
			done(err);
		}
		);

}

exports = module.exports = function(done) {
	async.forEach(AchievementItems, createAchievementItem, done);
};