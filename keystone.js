// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone');
//store the base path
global.__base = __dirname;


keystone.init({

	'name': 'playstudioCodeTest',
	'brand': 'playstudioCodeTest',
	
	'sass': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'jade',
	
	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'cookie secret': 'TKM0cSEzDM"xR<>"/*t?{AkCTQC)-%g^&t*eS:f5{T.accp5Kk1b^2=0H5kTI*!$'

});

keystone.import('models');

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	'users': 'users',
	'Achievement': ['AchievementItem', 'PlayerAchievementRecord', 'Reward']
});

keystone.start();
