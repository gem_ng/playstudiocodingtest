var keystone = require('keystone');
var Types = keystone.Field.Types;

var Utility = require(global.__base + "/helper/Utility");
var PlayerAction = require(global.__base + "/enum/PlayerAction");

var achievementsActionArrayStr = Utility.enumToKeystonOptionStr(PlayerAction);

var Enum = require("Enum");
var AchievementType = new Enum({
	"Daily":0,
	"Permanent":1
});
var achievementsTypeArrayStr = Utility.enumToKeystonOptionStr(AchievementType);


/**
 * User Model
 * ==========
 */

var AchievementItemModel = new keystone.List('AchievementItem');

AchievementItemModel.add({
	name: { type: Types.Text, initial: true, required: true, index: true },
	description: { type: Types.Text, initial: true, required: true, index: true },
	achievementAction: {type: Types.Select, options: achievementsActionArrayStr, initial: true, required: true},
	countNeed: { type: Types.Number, initial: true, required: true, index: true },
	reward: { type: Types.Relationship, ref: 'Reward', initial: true },
	achievementType: {type: Types.Select, options: achievementsTypeArrayStr, initial: true, required: true}
});

/**
 * Registration
 */

AchievementItemModel.defaultColumns = 'name, achievementAction, countNeed, reward, achievementType';
AchievementItemModel.register();


/*
* Public class and functions
*/
function AchievementItem(){

}
AchievementItem.prototype.getAllAchievementItem = function(callback){
	AchievementItemModel.model.find().exec(callback);
}

AchievementItem.prototype.getAllAchievementItemWithAction = function(action, callback){
	AchievementItemModel.model.find({achievementAction:action}).populate('reward').exec(callback);
}

module.exports = AchievementItem;