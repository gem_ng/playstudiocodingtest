var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */

var PlayerAchievementRecordModel = new keystone.List('PlayerAchievementRecord');

PlayerAchievementRecordModel.add({
	playerId: { type: Types.Key, initial: true, required: true, index: true },
	achievement: { type: Types.Relationship, ref: 'AchievementItem', initial: true, required: true, index: true},
	isAwardClaimed: {type: Types.Boolean,initial: true, required: true},
	reward: { type: Types.Relationship, ref: 'Reward', initial: true }
});



/**
 * Registration
 */

PlayerAchievementRecordModel.defaultColumns = 'playerId, achievement, count';
PlayerAchievementRecordModel.register();

/*
* Public class and functions
*/
function PlayerAchievementRecord(){

}

PlayerAchievementRecord.prototype.addAchievementRecordForPlayer = function(playerId, achievementObjectId, rewardObjectID, callback){

	var newPlayerAchievementRecord = new PlayerAchievementRecordModel.model(
	{
		playerId:playerId,
		achievement:achievementObjectId,
		isAwardClaimed:false,
		reward:rewardObjectID
	}
	);
	newPlayerAchievementRecord.save(callback);
}


PlayerAchievementRecord.prototype.getAchievementRecordForPlayer = function(playerId, callback){
	PlayerAchievementRecordModel.model.find({playerId:playerId}).populate("reward").exec(callback);
}

PlayerAchievementRecord.prototype.getAchievementRecordForPlayerWithAction = function(playerId, action, callback){
	PlayerAchievementRecordModel.model.find({playerId:playerId})
	.populate({
		path:"reward"
		,match:{achievementAction:action}
	})
	.exec(callback);
}


module.exports = PlayerAchievementRecord;