var keystone = require('keystone');
var Types = keystone.Field.Types;


var Utility = require(global.__base + "/helper/Utility");
var Enum = require("Enum");
var RewardType = new Enum({
	"LoyalityPoint":0,
	"Chip":1,
	"Exp":2,
	"Collectible":3
}, { separator: ' , ' });
var rewardArrayStr = Utility.enumToKeystonOptionStr(RewardType);

/**
 * User Model
 * ==========
 */

var RewardModel = new keystone.List('Reward');

RewardModel.add({
	name: { type: Types.Text, required: true, index: true },
	count: { type: Types.Number, initial: true, required: true, index: true },
	rewardType: {type: Types.Select, options: rewardArrayStr, initial: true, required: true, index: true}
});




/**
 * Registration
 */

RewardModel.defaultColumns = 'name, count, rewardType';
RewardModel.register();



/*
* Public class and functions
*/
function Reward(){

}

Reward.prototype.getRewardWithName = function(name, callback){
	RewardModel.model.findOne({name:name}).exec(callback);
}


module.exports = Reward;