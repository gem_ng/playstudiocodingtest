var keystone = require('keystone');
var Types = keystone.Field.Types;

var Utility = require(global.__base + "/helper/Utility");
var PlayerAction = require(global.__base + "/enum/PlayerAction");

var playerActionArrayStr = Utility.enumToKeystonOptionStr(PlayerAction);


/**
 * User Model
 * ==========
 */

var PlayerActionStatsModel = new keystone.List('PlayerActionStats');

PlayerActionStatsModel.add({
	playerId: { type: Types.Text, initial: true, required: true, index: true },
	action: {type: Types.Select, options: playerActionArrayStr, initial: true, required: true},
	count: { type: Types.Number, initial: true, required: true, index: true },
});



/**
 * Registration
 */

PlayerActionStatsModel.defaultColumns = 'playerId, action, count';
PlayerActionStatsModel.register();


/*
* Public class and functions
*/
function PlayerActionStats(){

}
PlayerActionStats.prototype.getAllPlayerActionStatsForPlayer = function(playerId, callback){
	PlayerActionStatsModel.model.find({playerId:playerId}).exec(callback);
}

PlayerActionStats.prototype.updateActionStatsForPlayer = function(playerId, action, countToAdd, callback){
	console.log("PlayerActionStats.updateActionStatsForPlayer");
	PlayerActionStatsModel.model.findOneAndUpdate(
		{playerId:playerId,action:action},
		{playerId:playerId,action:action,$inc:{count:parseInt(countToAdd)}},
		{upsert:true}
		).exec(callback);
}



module.exports = PlayerActionStats;